package Service;

import Model.User;
import org.junit.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UserServiceTest {

    private static EntityManagerFactory entityManagerFactory ;
    private static UserService userService;
    private static EntityManager entityManager;

    @BeforeClass
    public static void beforeAll(){
        entityManagerFactory = Persistence.createEntityManagerFactory("unit");
        userService = new UserService();
    }

    @Before
    public void setUp(){
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Test
    public void testConnection(){
        assertTrue(entityManager.isOpen());
    }


    @Test
    public void addUser(){
       
        entityManager.getTransaction().begin();
        User user = User.builder().id(3L).email("abc@abc.com").password("test").build();
        userService.addUser(user);
        entityManager.getTransaction().commit();
        List<User> databaseUsers = userService.getAllUsers();
        assertEquals(1,databaseUsers.size());
       
    }

    @Test(expected = RollbackException.class)
    public void addExistentUser(){
        entityManager.getTransaction().begin();
        User user = User.builder().id(3L).email("abc@abc.com").password("test").build();
        userService.addUser(user);
        entityManager.getTransaction().commit();
        List<User> databaseUsers = userService.getAllUsers();
        assertEquals(1,databaseUsers.size());
    }

    @Test
    public void updateUser(){
       
        entityManager.getTransaction().begin();
        User userModified = User.builder().id(3L).email("updatedEmail@abc.com").password("updatedPassword").previousOrders(new ArrayList<>()).build();
        System.out.println(userModified.toString());
        userService.updateUser(3L, userModified);
        User actual = entityManager.find(User.class, 3L);
        assertEquals(actual.toString(),userModified.toString());
       
    }

    @Test
    public void deleteUser(){
       
        entityManager.getTransaction().begin();
        userService.deleteUser(3L);
        entityManager.getTransaction().commit();
        List<User> databaseUsers = userService.getAllUsers();
        assertEquals(0,databaseUsers.size());
       
    }

    @Test
    public void getAllUsers(){
       
        entityManager.getTransaction().begin();
        List<User> databaseUsers = userService.getAllUsers();
        assertEquals(1,databaseUsers.size());
       
    }

    @Test(expected = IllegalStateException.class)
    public void testGetInClosedDatabaseConnection(){
        entityManagerFactory.close();
        try{
            entityManager.getTransaction().begin();
        }catch(IllegalStateException e){
            entityManagerFactory = Persistence.createEntityManagerFactory("unit");
            throw new IllegalStateException();
        }

    }

    @After
    public void tearDown(){
        entityManager.close();
    }

    @AfterClass
    public static void afterClass(){
        entityManagerFactory.close();
    }
}