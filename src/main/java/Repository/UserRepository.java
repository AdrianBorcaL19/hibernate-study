package Repository;

import Model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

public class UserRepository {
    private EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("unit");

    public List<User> getAllUsers(){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        String select = "Select u FROM User u";
        Query query = entityManager.createQuery(select);
        entityManager.getTransaction().begin();
        List<User> users = query.getResultList();
        entityManager.close();
        return users;
    }
    public void addUser(User user){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }
    public void deleteUser(Long id){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        User toDelete = entityManager.find(User.class,id);
        entityManager.getTransaction().begin();
        entityManager.remove(toDelete);
        entityManager.getTransaction().commit();
        entityManager.close();
    }
    public void updateUser(Long id,User modified){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        User toUpdate = entityManager.find(User.class,id);
        entityManager.getTransaction().begin();
        entityManager.merge(toUpdate);
        toUpdate.setEmail(modified.getEmail());
        toUpdate.setPassword(modified.getPassword());
        toUpdate.setPreviousOrders(modified.getPreviousOrders());
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public boolean contains(User user){
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        boolean exists = entityManager.contains(user);
        entityManager.getTransaction().commit();
        entityManager.close();
        return exists;
    }
}
