package Service;


import Model.User;
import Repository.UserRepository;

import java.util.List;

public class UserService {

    private final UserRepository userRepository = new UserRepository();
    public void addUser(User user){
        userRepository.addUser(user);
    }
    public void updateUser(Long id, User modified){
        userRepository.updateUser(id,modified);
    }
    public void deleteUser(Long id){
        userRepository.deleteUser(id);
    }
    public List<User> getAllUsers(){
        return userRepository.getAllUsers();
    }
    public boolean isUserInDatabase(User user){
        return userRepository.contains(user);
    }
}
