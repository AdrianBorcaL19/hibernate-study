package Model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString

@Table(name = "users")
public class User {
    @Id
    private Long id;
    @Column
    private String email;
    @Column
    private String password;
    @Column
    @OneToMany
    private List<Orders> previousOrders;
}
