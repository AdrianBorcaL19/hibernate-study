package Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.List;

@Entity
public class Orders {
    @Id
    private Long id;

    @ManyToMany
    private List<Product> cartlist;

    @ManyToOne
    private User user;
}
