package Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.List;

@Entity
public class Product {
    @Id
    private Long id;
    private float price;
    private String brand;
    @ManyToMany
    private List<Orders> orders;
}
